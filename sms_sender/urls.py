from django.urls import path
from sms_sender import views

urlpatterns = [
    path(r'^api/message', views.message),
]
