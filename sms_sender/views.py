from django.http import JsonResponse

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from configs.config import Settings
from sms_sender.helpers.generate_message import generate_message
from sms_sender.helpers.handle_phone import handle_phone
from sms_sender.serializers import MessageSerializer


@api_view(['POST'])
def message(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        data['phone'] = handle_phone(data['phone'])
        data['message'] = generate_message(Settings.code_length, Settings.message_type)
        if data['phone'] == 0:
            return JsonResponse(data={'result': f'{data["phone"]} denied'},
                                status=status.HTTP_400_BAD_REQUEST)
        serializer = MessageSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            print(serializer.data['message'])
            return JsonResponse(data={'result': 'ok'}, status=status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
