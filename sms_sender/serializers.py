from rest_framework import serializers
from sms_sender.models import Message


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('id',
                  'message',
                  'phone_number',
                  'created_at')
