from django.db import models


class Message(models.Model):
    message = models.CharField(max_length=6)
    phone_number = models.BigIntegerField()
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
