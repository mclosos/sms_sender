import phonenumbers

import configs.config


def handle_phone(phone: str) -> int:
    """
    Handle and return phone as an integer
    :param phone: phone number
    :return: int
    """
    if phone[0] == '+':
        phone = phonenumbers.parse(phone)
    else:
        phone = phonenumbers.parse(f'+{phone}')
    if phone.country_code in configs.config.Limits.blocked_countries_codes:
        return 0
    return phone.national_number
