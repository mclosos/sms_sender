from random import choice


def generate_message(length: int, message_type: str) -> str:
    return ''.join(choice(message_type) for _ in range(length))
