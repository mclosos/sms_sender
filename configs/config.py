import os
from string import digits, hexdigits

from configs.country_codes import country_codes

BLOCKED_COUNTRIES = ('KZ',)


class Limits:
    timeout = 30
    blocked_countries_codes = [country['code'] for country in country_codes if country['name'] in BLOCKED_COUNTRIES]


class Settings:
    code_length = 6
    if os.getenv('CODETYPE') == 'integer':
        message_type = digits
    elif os.getenv('CODETYPE') == 'string':
        message_type = hexdigits
