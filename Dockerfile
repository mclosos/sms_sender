FROM python:3.9-slim
RUN apt-get update
RUN apt-get install -y gnupg2 \
    software-properties-common \
    wget \
    python3-dev \
    libpq-dev \
    libevent-dev \
    gcc
ENV LANG C.UTF-8
COPY ${PWD} "/www/sms_sender"
ENV PYTHONPATH="/www/sms_sender"
WORKDIR "/www/sms_sender"
RUN pip install -r requirements.txt
EXPOSE 8080
