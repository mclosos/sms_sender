### Run app

```shell
CODETYPE='integer' python manage.py runserver 8080
```


```shell
CODETYPE='integer'  docker build -t sms_sender . && docker run --rm sms_sender python manage.py runserver 8080
```
or

```shell
CODETYPE='integer' PROJECT_PATH="`pwd`" docker-compose up
```
